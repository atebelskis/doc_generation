from django.views.generic import CreateView
from .forms import CreateIncomeForm, CreateIncomeProductForm, \
    CreateOrderForm, CreateOrderLineForm, CreateProductForm, CreateCategoryForm, CreateCustomerForm
from django.shortcuts import render
from .models import Income, IncomeProduct, Product, Category, Order, OrderLine, Customer


def main_page(request):
    return render(request, 'home.html')


def add_income(request):
    if request.method == "POST":
        income_form = CreateIncomeForm(request.POST, instance=Income())
        income_product_form = CreateIncomeProductForm(request.POST, instance=IncomeProduct())
        product_form = CreateProductForm(request.POST, instance=Product())
        category_form = CreateCategoryForm(request.POST, instance=Category())
        income_valid = income_form.is_valid()
        income_product_valid = income_product_form.is_valid()
        product_valid = product_form.is_valid()
        category_valid = category_form.is_valid()
        if income_valid and income_product_valid and product_valid and category_valid:
            income = income_form.save()
            income_product = income_product_form.save()
            product = product_form.save()
            category = category_form.save()
            product.cat = category
            product.save()
            income_product.income = income
            income_product.product = product
            income_product.save()

    else:
        income_form = CreateIncomeForm(instance=Income())
        income_product_form = CreateIncomeProductForm(instance=IncomeProduct())
        product_form = CreateProductForm(instance=Product())
        category_form = CreateCategoryForm(instance=Category())

    context = {'income_tamplate': income_form,
               'income_product': income_product_form,
               'product': product_form,
               'category': category_form}

    return render(request, 'income.html', context)

def add_order(request):
    if request.method == "POST":
        customer_form = CreateCustomerForm(request.POST, instance=Customer())
        order_form = CreateOrderForm(request.POST, instance=Order())
        order_line_form = CreateOrderLineForm(request.POST, instance=OrderLine())
        customer_form_valid = customer_form.is_valid()
        order_form_valid = order_form.is_valid()
        order_line_form_valid = order_line_form.is_valid()
        if customer_form_valid and order_form_valid and order_line_form_valid:
            customer = customer_form.save()
            order = order_form.save()
            order_line = order_line_form.save()
            order_line.order = order
            # income_product.product = product
            # income_product.save()
    else:
        order_form = CreateOrderForm(instance=Order())
        order_line_form = CreateOrderLineForm(instance=OrderLine())
        customer_form = CreateCustomerForm(instance=Customer())

    context = {'order': order_form, 'order_line': order_line_form, 'customer': customer_form}

    return render(request, 'order.html', context)


# class IncomeView(CreateView):
#     form_class = CreateIncomeForm
#     model = Income
#     template_name = 'income.html'
#     success_url = '/'


# class IncomeProductView(CreateView):
#     form_class = CreateIncomeProductForm
#     model = IncomeProduct
#     template_name = 'income_product.html'
#     success_url = '/'
#
#
# class OrderView(CreateView):
#     form_class = CreateOrderForm
#     model = Order
#     template_name = 'order.html'
#     success_url = '/'

def add_stock(request):
    context = {'stock': 'testas'}
    return render(request, 'stock.html', context)