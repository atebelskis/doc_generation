from django.contrib import admin
from .models import *

admin.site.register(Income)
admin.site.register(IncomeProduct)
admin.site.register(Product)
admin.site.register(Category)
admin.site.register(Stock)
admin.site.register(OrderLine)
admin.site.register(Order)
admin.site.register(Customer)


