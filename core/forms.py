from django.forms import ModelForm
from .models import Income, IncomeProduct, Order, OrderLine, Product, Category, Customer


class CreateIncomeForm(ModelForm):
    class Meta:
        model = Income
        fields = '__all__'


class CreateIncomeProductForm(ModelForm):
    class Meta:
        model = IncomeProduct
        fields = '__all__'


class CreateProductForm(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'


class CreateCategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = '__all__'


class CreateOrderLineForm(ModelForm):
    class Meta:
        model = OrderLine
        fields = '__all__'


class CreateCustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = '__all__'


class CreateOrderForm(ModelForm):
    class Meta:
        model = Order
        fields = '__all__'
