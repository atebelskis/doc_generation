from django.db import models


class Category (models.Model):
    cat_id = models.AutoField(primary_key=True)
    cat_name = models.CharField(max_length=50)

    class Meta:
        db_table = "Category"

    def __str__(self):
        return self.cat_name


class Product (models.Model):
    product_id = models.AutoField(primary_key=True)
    product_name = models.CharField(max_length=50)
    cat = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        db_table = "Product"

    def __str__(self):
        return self.product_name


class Income (models.Model):
    income_id = models.AutoField(primary_key=True)
    date = models.DateField()

    class Meta:
        db_table = "Income"

    def __str__(self):
        return f'{self.date}'


class IncomeProduct(models.Model):
    income_product_id = models.AutoField(primary_key=True)
    income = models.ForeignKey(Income, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    class Meta:
        db_table = "IncomeProduct"



class Stock (models.Model):
    stock_id = models.AutoField(primary_key=True)
    date = models.DateField()
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    class Meta:
        db_table = "Stock"

    def __str__(self):
        return f'{self.date}'


class Customer(models.Model):
    customer_name = models.CharField(max_length=50)
    registration_date = models.DateField()
    customer_id = models.AutoField(primary_key=True)

    class Meta:
        db_table = "Customer"

    def __str__(self):
        return self.customer_name


class Order(models.Model):
    order_id = models.AutoField(primary_key=True)
    date = models.DateField()
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)

    class Meta:
        db_table = "Order"

    def __str__(self):
        return f'{self.date}'


class OrderLine(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    order_lines_id = models.AutoField(primary_key=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    price = models.FloatField()
    quantity = models.IntegerField()

    class Meta:
        db_table = "OrderLine"
