from django.urls import path
from .views import add_income, main_page, add_order, add_stock

urlpatterns = [
    path('', main_page, name='home'),
    path('income/', add_income, name='income'),
    # path('income-product/', IncomeProductView.as_view(), name='income'),
    path('order/', add_order, name='order'),
    path('stock/', add_stock, name='stock')
]