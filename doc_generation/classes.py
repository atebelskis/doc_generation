import uuid

class Income:
    def __init__(self, income_product, date, income_id):
        self.income_product = income_product
        self.date = date
        self.income_id = income_id

class IncomeProduct:
    def __init__(self, income_product_id, income_id, product_id, quantity):
        self.income_product_id = income_product_id # 001- pajamuoto produkto iraso nr.
        self.income_id = income_id # pajam. dokumento nr.
        self.product_id = product_id # producto kodas
        self.quantity = quantity # produkto kiekis

class Products:
    def __init__(self, product_id, product_name, cat_id):
        self.product_id = product_id
        self.product_name = product_name
        self.cat_id = cat_id

class Categories:
    def __init__(self, cat_id, cat_name):
        self.cat_id = cat_id
        self.cat_name = cat_name

class Stock:
    def __init__(self, product_id, date, quantity):
        self.product_id = product_id
        self.date = date
        self.quantity = quantity

class Customers:
    def __init__(self, customer_name, registration_date, customer_id):
        self.customer_name = customer_name
        self.registration_date = registration_date
        self.customer_id = customer_id

class Orders:
    def __init__(self, order_line, order_id, date, customer_id):
        self.order_line = order_line
        self.order_id = order_id
        self.date = date
        self.customer_id = customer_id

class OrderLines:
    def __init__(self, orders_id, order_line_id, product_id, quantity):
        self.order_id = orders_id
        self.order_line_id = order_line_id
        self.product_id = product_id
        self.quantity = quantity
