
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

DATABASES = {
    'default': {
        'ENGINE': 'db_incomes_id',
        'NAME': BASE_DIR / 'db_incomes_id',
    }
}
