
from core.models import *


def check_stock_level(stock, order, income, income_date):

    latest_stock_date = stock.objects.latest('date').date
    latest_stock = stock.objects.filter(date=latest_stock_date)
    latest_orders = order.objects.filter(date__lte=income_date, date__gte=latest_stock_date)
    latest_incomes = income.objects.filter(date__lte=income_date, date__gte=latest_stock_date)
    new_stock_products = []

    for order in latest_orders.objects.all():
        order_product_id = order.orderline.product.product_id
        for income in latest_incomes.objects.all():
            income_product_id = income.incomeproduct.product.product_id
            if order_product_id == income_product_id:
                order_product_quantity = order.orderline.filter(product__product_id=order_product_id).quantity
                income_product_quantity = income.incomeproduct.filter(product__product_id=income_product_id).quantity
                stock_product_quantity = latest_stock.filter(product__product_id=order_product_id).quantity
                stock_new_product_quantity = stock_product_quantity - order_product_quantity + income_product_quantity
                new_stock_product = Stock(date=income_date, product=income.incomeproduct.product,
                                          quantity=stock_new_product_quantity)
                new_stock_products.append(new_stock_product)

        return new_stock_products




















