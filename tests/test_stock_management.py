import datetime
import pytest

from core.models import Stock, Order, Income, IncomeProduct, OrderLine, Customer, Product, Category
from doc_generation.stock_management import check_stock_level

@pytest.mark.django_db
def test_check_stock_level():
    category1 = Category(cat_name="CATEGORY1").save()
    category2 = Category(cat_name="CATEGORY2").save()

    product1 = Product.objects.create(product_name="PRODUCT1", cat=category1)
    product2 = Product.objects.create(product_name="PRODUCT2", cat=category2)

    customer1 = Customer(customer_name="John", registration_date=datetime.date(2021, 1, 1))

    stock1 = Stock(date=datetime.date(2022, 1, 1), product=product1, quantity=100)  # Kiekis produkto sandelyje

    # Uzsakymo data su kliento ID

    order1 = Order.objects.create(date=datetime.date(2022, 9, 1), customer=customer1)
    order2 = Order.objects.create(date=datetime.date(2022, 7, 1), customer=customer1)
    order3 = Order.objects.create(date=datetime.date(2022, 8, 1), customer=customer1)

    # Pajamavimo data

    income1 = Income(date=datetime.date(2022, 9, 1))
    income2 = Income(date=datetime.date(2022, 8, 1))
    income3 = Income(date=datetime.date(2022, 10, 1))

    # pajamavimo orderis, produktas ir jo kiekis

    income_product1 = IncomeProduct.objects.create(income=income1, product=product1, quantity=22)
    income_product2 = IncomeProduct.objects.create(income=income2, product=product2, quantity=33)
    income_product3 = IncomeProduct.objects.create(income=income3, product=product1, quantity=11)

    # uzsakymo nr., produktas, kaina ir produkto kiekiss

    order_line1 = OrderLine.objects.create(order=order1, product=product1, price=11.1, quantity=22)
    order_line2 = OrderLine.objects.create(order=order2, product=product2, price=2.5, quantity=30)
    order_line3 = OrderLine.objects.create(order=order3, product=product1, price=10.5, quantity=55)

    income_date = datetime.date(2022, 9, 2)

    output = check_stock_level(stock1, [order1, order2, order3], [income1, income2, income3])
    expected = [Stock("1234", income_date, 45), Stock("2345", income_date, 3)]
    assert output == expected

